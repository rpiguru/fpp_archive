from django.db import models
from ckeditor.fields import RichTextField
from django.core.urlresolvers import reverse
from django.utils.text import slugify

# Create your models here.

class Course(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField()
    subtitle = models.TextField()
    banner = models.ImageField(
        upload_to='courses/banners',
    )

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('courses:toc', args=[self.slug])


class Lesson(models.Model):
    title = models.CharField(max_length=255)
    section_number = models.IntegerField()
    day_number = models.IntegerField()
    course = models.ForeignKey(Course)
    content = RichTextField(
        config_name='courses_ckeditor',
    )
    position = models.PositiveSmallIntegerField("Position")

    class Meta:
        ordering = ['position']

    def __str__(self):
        return str(self.section_number) + '.' + str(self.day_number) + ' ' + self.title
