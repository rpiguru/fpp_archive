from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.courses, name='index'),
    url(r'^(?P<course_name>[\w-]+)/$', views.toc, name='toc'),
    url(r'^(?P<course_name>[\w-]+)/(?P<course_day>[0-9]+)/$',
        views.single, name='single'),
    url(r'^(?P<filetype>epub)/(?P<course_name>[\w-]+)/$', views.epub_gen, name='epub'),
    url(r'^auth/register/$', views.register, name='register'),
]
