from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class RegistrationForm(UserCreationForm):
    next = forms.CharField(initial='{{ next }}', widget=forms.HiddenInput)
    class Meta:
        model = User
        fields = ("username", "email")
