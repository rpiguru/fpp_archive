from django.contrib import admin
from django.utils.text import slugify
from .models import Course, Lesson
from grappelli.forms import GrappelliSortableHiddenMixin

# Register your models here.

class LessonInline(GrappelliSortableHiddenMixin, admin.StackedInline):
    model = Lesson
    extra = 0

@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    inlines = [
        LessonInline,
    ]
    prepopulated_fields = {'slug': ('title',),}

