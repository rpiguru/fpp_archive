# FPP_Archive (www.fpparchive.org)
==========================================

## Install dependencies

    sudo apt-get install libxml2-dev libxslt1-dev python-dev
    sudo pip install -r requirements.txt

## Run server
    
Before running server, please check `fpp/settings.py` to set correct static paths.

- HTTP server:

        python manage.py runserver
    
    If you need to run server on port 80, please just execute the command above with `sudo` prefix.
    
- HTTPS server:
        
        python manage.py runsslserver --certificate cert/www.fpparchive.org.crt --key cert/www.fpparchive.org.key
    
    If you need to run server on port 443, please just execute the command above with `sudo` prefix.
    

## Execute live server with nginx+gunicorn
        
        cd ~/fpp_archive
        python manage.py collectstatic --noinput
        gunicorn fpp.wsgi -b 127.0.0.1:8002 --daemon -w1 --log-file=out.log
        
        sudo apt-get install nginx
        sudo cp nginx-http.conf /etc/nginx/sites-available/fpp
        sudo ln -s /etc/nginx/sites-available/fpp /etc/nginx/sites-enable/fpp
        sudo service nginx restart
        