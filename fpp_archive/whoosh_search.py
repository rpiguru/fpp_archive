'''Whoosh Search engine implementation for the FPP Archive.'''
from datetime import datetime

from whoosh.analysis import StemmingAnalyzer
from whoosh.lang.porter import stem
from whoosh.filedb.filestore import FileStorage
from whoosh.fields import Schema, TEXT, ID, KEYWORD, DATETIME, NGRAMWORDS
from whoosh.qparser import FuzzyTermPlugin, MultifieldParser, OrGroup, QueryParser
from whoosh.writing import AsyncWriter

class FppIndex:
    '''A searchable document index.'''
    def __init__(self, index_dir):
        self.storage = FileStorage(index_dir)
        self.schema = Schema(
            doc_id=ID(unique=True, stored=True),
            title=TEXT(analyzer=StemmingAnalyzer(),
                       stored=True,
                       field_boost=3.0),
            url=TEXT(stored=True),
            content=TEXT(analyzer=StemmingAnalyzer()),
            author=TEXT(stored=True),
            publication=TEXT(stored=True),
            pub_date=TEXT(stored=True),
            keyword=KEYWORD(commas=True,
                            stored=True,
                            lowercase=True,
                            scorable=True,
                            field_boost=2.0),
            category=KEYWORD(commas=True,
                             stored=True,
                             lowercase=True,
                             scorable=True,
                             field_boost=2.0),
            timestamp=DATETIME(stored=True),
        )


    def create(self):
        '''Create the index.'''
        self.storage.create()

        if not self.storage.index_exists():
            self.storage.create_index(self.schema)
        else:
            pass #Throw ERROR


    def add(self,
            doc_id,
            title,
            url,
            content,
            dump=None,
            author=None,
            publication=None,
            pub_date=None,
            keyword=None,
            category=None):
        '''Add document to index.'''
        index = self.storage.open_index()
        writer = AsyncWriter(index)

        writer.update_document(
            doc_id=doc_id,
            title=title,
            url=url,
            content=content,
            author=author,
            publication=publication,
            pub_date=pub_date,
            keyword=keyword,
            category=category,
            timestamp=datetime.now()
        )

        writer.commit()


    def delete(self, fieldname, termtext):
        '''Remove document to index.'''
        index = self.storage.open_index()
        writer = AsyncWriter(index)
        writer.delete_by_term(fieldname, termtext)
        writer.commit()


    def search(self, user_query, fulltext=False):
        '''Search for user_query in index.'''
        index = self.storage.open_index()

        '''
        if not fulltext:
            temp_list = user_query.split()
            for temp_file in range(len(temp_list)):
                temp_list[temp_file] += '~'
            user_query = ' '.join(temp_list)
            '''

        with index.searcher() as searcher:
            if fulltext:
                parser = QueryParser('content', schema=index.schema)
                query = parser.parse(user_query)
            else:
                og = OrGroup.factory(0.9)
                parser = MultifieldParser(
                    ['title', 'category', 'keyword', 'author', 'publication'],
                    schema=index.schema,
                    group=og
                )
             #   parser.add_plugin(FuzzyTermPlugin())
                query = parser.parse(user_query)

            results = searcher.search(query, limit=None)

            output = []
            for result in results:
                interim = {}
                for hit in result:
                    interim[hit] = result[hit]
                output.append(interim)

        return output

    def cat_search(self, user_query):
        '''Search for user_query in index.'''
        index = self.storage.open_index()

        with index.searcher() as searcher:
            parser = QueryParser('category', schema=index.schema)
            query = parser.parse(user_query)

            results = searcher.search(query, limit=None)

            output = []
            for result in results:
                interim = {}
                for hit in result:
                    interim[hit] = result[hit]
                output.append(interim)

        return output
