var numberOfImages;
var imageHeights = [];
var imageWidths = [];
var gridSize;
function grid(width) {
	var siteWidth = width;
	var rowLength = 0;
	var marker= 0;
	var item;
	var counter;
	var percentageToFill;
	var amountToRemove;
	for (var i=1; i <= numberOfImages; i++)
	{
		rowLength = rowLength + imageSize(i)[0];
		if (rowLength > siteWidth)
		{
			if (siteWidth < (rowLength - (imageSize(i)[0]/2))) {
				rowLength = rowLength - imageSize(i)[0];
				i--;
				percentageToFill = (((siteWidth - 15 *  (i - marker))/rowLength)) - 1;
				counter = i - marker;
				item = 0;
				for (var index=1; index <= counter; index++)
				{
					item = (i - index + 1);
					sizes = imageSize(item);
					modifyImage(item, (sizes[0] * percentageToFill), (sizes[1] * percentageToFill));
				}
				marker = i;
				rowLength = 0;
			}
			else {
				amountToRemove = ((((siteWidth - 15 *  (i - marker))/rowLength)) - 1) * -1;
				counter = i - marker;
				item = 0;
				for (var loopIndex=1; loopIndex <= counter; loopIndex++) {
					item = (i - loopIndex + 1);
					sizes = imageSize(item);
					modifyImage(item, (sizes[0] * amountToRemove), (sizes[1] * amountToRemove), true);
				}
				rowLength = 0;
				marker = i;
			}
		}
	}

	//Handles any extra Images off the end
	if (marker != numberOfImages) {
		if ((numberOfImages - marker) > 0)
		{
			rowLength = 0;
			for (var index=marker + 1; index <= numberOfImages; index++) {
				rowLength = rowLength + imageSize(index)[0];
			}
			if (rowLength > siteWidth * .7 ) {
				percentageToFill = (((siteWidth - 15 *  (numberOfImages - marker))/rowLength)) - 1;
				for (var index=marker + 1; index <= numberOfImages; index++) {
					sizes = imageSize(index);
					modifyImage(index, (sizes[0] * percentageToFill), (sizes[1] * percentageToFill));
				}
			} else {
				percentageToFill = ((((siteWidth / 2) - 15 *  (numberOfImages - marker))/rowLength)) - 1;
				for (var index=marker + 1; index <= numberOfImages; index++) {
					sizes = imageSize(index);
					modifyImage(index, (sizes[0] * percentageToFill), (sizes[1] * percentageToFill));
				}
			}
		}
	}
}

//Gets The Image Size
function imageSize(selector){
	var width = jQuery('.grid img:nth-child(' + selector + ')').width();
	var height = jQuery('.grid img:nth-child(' + selector + ')').height();
	return [width, height];
}

function modifyImage(selector, width, height, remove){
	remove = typeof remove !== 'undefined' ? remove : false;
	if (remove === false) {
		jQuery('.grid img:nth-child(' + selector + ')').width('+=' + width);
		jQuery('.grid img:nth-child(' + selector + ')').height('+=' + height);
	} else {
		jQuery('.grid img:nth-child(' + selector + ')').width('-=' + width);
		jQuery('.grid img:nth-child(' + selector + ')').height('-=' + height);
	}
}

function saveImageSizes() {
	for (var index = 1; index <= numberOfImages; index++) {
		imageWidths.push(imageSize(index)[0]);
		imageHeights.push(imageSize(index)[1]);
	}
}

function reloadImages() {
	for (var i = 0; i < imageHeights.length; i++) {
    	jQuery('.grid img:nth-child(' + (i+1) + ')').width(imageWidths[i]);
    	jQuery('.grid img:nth-child(' + (i+1) + ')').height(imageHeights[i]);
	}
	grid(gridSize);
}

// Initial Grid Run
jQuery( document ).ready(function() {
	jQuery(window).load(function() {
		numberOfImages = jQuery(".grid img").length;
		saveImageSizes();
		gridSize = jQuery('.grid').width();
		grid(gridSize);
	});
});

//Resize on page size
var timer;
window.onresize = function() {
	if (jQuery('.grid').width() != gridSize) {
	    clearTimeout(timer);
	    timer = setTimeout(function() {
	    	gridSize = jQuery('.grid').width();
	    	reloadImages();
	    }, 50);
	}
};
