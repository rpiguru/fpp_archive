from django.db import models

# Create your models here.
class Keyword(models.Model):
    keyword = models.CharField(max_length=255)

    def __str__(self):
        return self.keyword

class Category(models.Model):
    category = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.category

class Document(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255, blank=True)
    publication = models.CharField(max_length=255, blank=True)
    publication_date = models.CharField(max_length=255, blank=True)
    file = models.FileField(max_length=255, upload_to='documents/%Y/%m/%d/')
    keywords = models.ManyToManyField(Keyword)
    categories = models.ManyToManyField(Category)

    def __str__(self):
        return self.title
